import * as crypto from 'crypto';
import { flatten, unflatten } from 'flatley';
import * as mongoose from 'mongoose';
import * as _ from 'underscore';

const coercion = [
  {
    test(key: string, value: any) {
      return (
        typeof value === 'object' && mongoose.Types.ObjectId.isValid(value)
      );
    },
    transform(value: mongoose.Types.ObjectId) {
      return value.toString();
    }
  }
];

export function encrypt(text: string, key: string): string {
  const cipher = crypto.createCipher('aes-256-cbc', key);
  return `${cipher.update(text, 'utf8', 'hex')}${cipher.final('hex')}`;
}

export function decrypt(text: string, key: string): string {
  const decipher = crypto.createDecipher('aes-256-cbc', key);
  return `${decipher.update(text, 'hex', 'utf8')}${decipher.final('utf8')}`;
}

export function parseAuthHeader(
  header: string
): { schema: string; token: string } {
  const matches = (header || '').match(/(\S+)\s+(\S+)/) || [];
  return { schema: matches[1], token: matches[2] };
}

export function jsonFilter(
  params: string[],
  opts?: mongoose.DocumentToObjectOptions
): any {
  const data = this.toJSON(opts);
  return unflatten(_.omit(flatten(data, { coercion }), params));
}

export default {
  encrypt,
  decrypt,
  parseAuthHeader,
  jsonFilter
};
