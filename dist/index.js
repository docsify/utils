"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const flatley_1 = require("flatley");
const mongoose = require("mongoose");
const _ = require("underscore");
const coercion = [
    {
        test(key, value) {
            return (typeof value === 'object' && mongoose.Types.ObjectId.isValid(value));
        },
        transform(value) {
            return value.toString();
        }
    }
];
function encrypt(text, key) {
    const cipher = crypto.createCipher('aes-256-cbc', key);
    return `${cipher.update(text, 'utf8', 'hex')}${cipher.final('hex')}`;
}
exports.encrypt = encrypt;
function decrypt(text, key) {
    const decipher = crypto.createDecipher('aes-256-cbc', key);
    return `${decipher.update(text, 'hex', 'utf8')}${decipher.final('utf8')}`;
}
exports.decrypt = decrypt;
function parseAuthHeader(header) {
    const matches = (header || '').match(/(\S+)\s+(\S+)/) || [];
    return { schema: matches[1], token: matches[2] };
}
exports.parseAuthHeader = parseAuthHeader;
function jsonFilter(params, opts) {
    const data = this.toJSON(opts);
    return flatley_1.unflatten(_.omit(flatley_1.flatten(data, { coercion }), params));
}
exports.jsonFilter = jsonFilter;
exports.default = {
    encrypt,
    decrypt,
    parseAuthHeader,
    jsonFilter
};
//# sourceMappingURL=index.js.map