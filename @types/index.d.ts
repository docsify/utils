import * as mongoose from 'mongoose';

declare namespace utils {
  export function encrypt(text: string, key: string): string;

  export function decrypt(text: string, key: string): string;

  export function parseAuthHeader(header: string): { schema: string; token: string };

  export function jsonFilter(params: string[], opts?: mongoose.DocumentToObjectOptions): any;
}

export = utils;
